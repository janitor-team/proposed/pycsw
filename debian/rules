#!/usr/bin/make -f
# -*- makefile -*-

export PYBUILD_NAME=pycsw
export PYBUILD_TEST_CUSTOM=1
export PYBUILD_TEST_ARGS=cd {dir} && PYTHONPATH={dir} {interpreter} -m pytest -v -k 'not harvesting' tests/
export PYBUILD_INSTALL_ARGS=--install-scripts=/usr/lib/{package}

include /usr/share/dpkg/pkg-info.mk

BUILD_DATE := $(shell LC_ALL=C date -u "+%Y-%m-%d" -d "@$(SOURCE_DATE_EPOCH)")

%:
	dh  $@ --with python3,sphinxdoc,apache2 --buildsystem pybuild

override_dh_clean:
	dh_clean debian/man/pycsw-admin.1

override_dh_auto_build:
	dh_auto_build
	PYTHONPATH=. http_proxy='127.0.0.1:9' sphinx-build -N -bhtml docs/ build/html # HTML generator
	ronn -r --date="$(BUILD_DATE)" --manual=pycsw debian/man/*.md
	rm -f debian/man/*.html

override_dh_auto_test:
	dh_auto_test || echo "Ignoring test failures"

override_dh_install:
	dh_install

	# Remove pycache directories
	$(RM) -r debian/*/usr/share/pycsw/tests/__pycache__/
	$(RM) -r debian/*/usr/share/pycsw/tests/functionaltests/__pycache__/
	$(RM) -r debian/*/usr/share/pycsw/tests/unittests/__pycache__/

	# Remove empty directories
	rmdir debian/*/usr/share/pycsw/tests/functionaltests/suites/harvesting/data/
	rmdir debian/*/usr/share/pycsw/tests/functionaltests/suites/manager/data/

	# Remove documentation outside usr/share/doc
	$(RM) debian/*/usr/share/pycsw/tests/README.txt
	$(RM) debian/*/usr/share/pycsw/tests/functionaltests/suites/apiso/data/README.txt
	$(RM) debian/*/usr/share/pycsw/tests/functionaltests/suites/cite/data/README.txt

override_dh_python3:
	dh_python3
	dh_python3 -p pycsw-wsgi --shebang=/usr/bin/python3 /usr/share/pycsw
